// res.set("Content-Type", "application/javascript");
//Функція пошуку

function redirectToPage(event) {
  event.preventDefault(); // Зупиняємо дію за умовчанням (відправку форми)

  const query = document.getElementById("searchInput").value.toLowerCase();

  // Визначаємо правила для переходу на сторінки в залежності від запиту
  switch (query) {
    // Додайте інші варіанти за потреби
    default:
      window.location.href = "./homepage.html";
      break;
  }
}
